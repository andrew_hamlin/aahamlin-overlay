# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-crypt/gorilla/gorilla-1.4.ebuild,v 1.4 2011/02/13 19:00:19 hwoarang Exp $

EAPI=6

inherit eutils

DESCRIPTION="Password Safe clone for Linux. Stores passwords in secure way with GUI interface."
HOMEPAGE="https://github.com/zdia/gorilla/wiki"
SRC_URI="https://github.com/zdia/gorilla/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND=">=dev-lang/tcl-8.4.19
	>=dev-lang/tk-8.4.19
	dev-tcltk/iwidgets
	dev-tcltk/bwidget"
RDEPEND=${DEPEND}

src_prepare() {
	eapply "${FILESDIR}/gorilla-tclsh.patch"
	eapply_user
}

src_configure() { :; }

src_install() {
	PREFIX="/opt/${P}"

	cd sources

	dodir ${PREFIX}
	insinto ${PREFIX}
	doins gorilla.tcl isaac.tcl
	doins -r gorilla.tcl isaac.tcl viewhelp.tcl blowfish itcl3.4 modules msgs pics pwsafe tcllib twofish
	fperms 0555 ${PREFIX}/gorilla.tcl
	dosym ${PREFIX}/gorilla.tcl /usr/bin/gorilla
	make_desktop_entry "gorilla" "Password Gorilla" "${PREFIX}/pics/gorilla-48x48.gif" Utility

}
