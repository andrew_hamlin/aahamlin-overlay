# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit eutils versionator

MY_PN="jetty"
MY_PV="${PV/_p/.v}"
MY_P="${MY_PN}-distribution-${MY_PV}"
MY_JETTY="${MY_PN}-${SLOT}"

DESCRIPTION="Jetty Web Server; Java Servlet container"
HOMEPAGE="http://www.eclipse.org/jetty/"
KEYWORDS="~amd64"
LICENSE="Apache-2.0"
SLOT="9"
SRC_URI="http://download.eclipse.org/jetty/stable-9/dist//${MY_P}.tar.gz"

IUSE=""

DEPEND=""
RDEPEND="${DEPEND}
	>=virtual/jre-1.7"

S="${WORKDIR}/${MY_P}"

src_install() {
	insinto "/etc/${MY_JETTY}"
	doins etc/*
	doins "start.ini"

	newconfd "${FILESDIR}/${MY_PN}.confd" "${MY_JETTY}"
	newinitd "${FILESDIR}/${MY_PN}.initd" "${MY_JETTY}"

	JETTY_HOME="/opt/${MY_JETTY}"

	keepdir "/var/log/${MY_JETTY}"

	insinto "${JETTY_HOME}"
	doins -r lib start.jar

	dodir "${JETTY_HOME}/webapps"
	dodir "${JETTY_HOME}/contexts"
	dodir "${JETTY_HOME}/resources"

	dosym "/var/log/${MY_JETTY}" "${JETTY_HOME}/logs"
	dosym "/etc/${MY_JETTY}" "${JETTY_HOME}/etc"
}

pkg_preinst () {
	enewuser jetty
	fowners jetty:jetty "/var/log/${MY_JETTY}"
	fperms g+w "/var/log/${MY_JETTY}"
}

