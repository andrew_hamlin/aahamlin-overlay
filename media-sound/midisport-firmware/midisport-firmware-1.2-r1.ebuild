# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=4

inherit eutils

DESCRIPTION="Firmware loader for M-Audio MidiSport devices"
HOMEPAGE="http://usb-midi-fw.sourceforge.net/"
SRC_URI="http://sourceforge.net/projects/usb-midi-fw/files/${PN}/${PV}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86 ~amd64 ~x86"


RDEPEND=">=virtual/udev-196
         sys-apps/fxload"
DEPEND="${RDEPEND}"

src_prepare() {
	epatch "${FILESDIR}"/udev-configure.patch
	epatch "${FILESDIR}"/udev-rules.patch
}

src_configure() {
	if [[ -x ${ECONF_SOURCE:-.}/configure ]]; then
		econf --with-udev=/lib/udev --with-fxload=/sbin/fxload
	fi
}
