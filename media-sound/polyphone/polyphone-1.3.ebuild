

EAPI=5

inherit eutils qt4-r2

DESCRIPTION='realtime audio synthesizer that allows you to "scratch" on digitally sampled audio data'
HOMEPAGE="http://polyphone.fr/"
SRC_URI="http://softlayer-dal.dl.sourceforge.net/project/polyphone/polyphone%20releases/${PV}/${PN}_${PV}.orig.tar.gz"

LICENSE="|| ( GPL-3+ GPL-2+ LGPL )"
SLOT="0"
KEYWORDS="~amd64 ~x86"


#IUSE="alsa mad vorbis sox"

#The following libraries are required (the name may vary depending on your system):
# - qt        (libqt4-dev / qt4-devel)
# - alsa      (libasound2-dev / alsa-lib-devel)
# - jack      (libjack-jack2-dev / jack-audio-connection-kit-devel)
# - portaudio (portaudio19-dev / portaudio-devel)
#Use your package manager to install them: synaptic, yum,...

RDEPEND="dev-qt/qtgui:4
	media-libs/alsa-lib
	media-sound/jack-audio-connection-kit[alsa]
	media-libs/portaudio[alsa,jack]"

DEPEND="${RDEPEND}
	sys-libs/zlib
	virtual/pkgconfig"


src_prepare() {
	epatch "${FILESDIR}"/${PN}.pro.patch
}

src_install() {
    # Install executable 
    exeinto /usr/bin
    doexe RELEASE/polyphone

    dodoc README
    dodoc debian/share/doc/${PN}/changelog.gz

    insinto /usr/share/mime/packages
    doins debian/share/mime/packages/${PN}.xml

    doicon debian/share/icons/${PN}.png
    domenu debian/share/applications/${PN}.desktop

}


