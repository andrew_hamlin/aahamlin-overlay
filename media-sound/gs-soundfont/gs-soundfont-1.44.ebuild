# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="The GeneralUser GS soundfont for fluidsynth"
HOMEPAGE="http://www.schristiancollins.com/generaluser.php"
SRC_URI="http://www.schristiancollins.com/soundfonts/GeneralUser_GS_${PV}-FluidSynth.zip
         musescore? ( http://www.schristiancollins.com/soundfonts/GeneralUser_GS_${PV}-MuseScore.zip )"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86 ~amd64 ~x86"
IUSE="musescore demo +instr"

RDEPEND="musescore? ( =media-sound/musescore-1.2 )"

DEPEND="app-arch/unzip"

MY_FLUIDSYNTH_DIR="GeneralUser GS ${PV} FluidSynth"
MY_MUSESCORE_DIR="GeneralUser GS ${PV} MuseScore"

S_MS="${S}/ms"

src_unpack() {
	cd ${T}
	unpack GeneralUser_GS_${PV}-FluidSynth.zip
	mv "${MY_FLUIDSYNTH_DIR}" ${S}
	if use musescore; then
		unpack GeneralUser_GS_${PV}-MuseScore.zip
		mv "${MY_MUSESCORE_DIR}" ${S_MS}
	fi
}

src_install() {
	insinto /usr/share/sounds/sf2
	doins *.sf2
	dodoc *.txt
	if use demo; then
		insinto /usr/share/${PF}
		doins *.mid	
		doins -r "demo MIDIs"
	fi
	if use instr; then
		insinto /usr/share/${PF}
		doins -r "instrument lists"
	fi
	if use musescore; then
		insinto /usr/share/mscore-1.2/sound
		doins ${S_MS}/*.sf2
		docinto musescore
		dodoc ${S_MS}/*.txt
		if use demo; then
			insinto /usr/share/mscore-1.2/demos
			doins ${S_MS}/*.mscz
		fi
	fi
}
